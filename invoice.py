try:
    import twain
    from tkinter import ttk
    from tkinter import *
    import tkinter as tk
    from tkinter.ttk import *
    import traceback, sys
    from tkinter import font
    from PIL import ImageTk
    import tempfile
    import img2pdf
    import os, errno
    import img2pdf
    import PIL.Image
    from fpdf import FPDF
    import pyodbc
    import random
    import getpass
    import shutil

    import Tkinter as tk
    import tkFont
    import ttk
except ImportError:
    import tkinter as tk
    import tkinter.font as tkFont
    import tkinter.ttk as ttk
    from tkinter import messagebox


class MultiColumnListbox(object):
    def __init__(self):
        self.tree = None
        self._setup_widgets()
        self._build_tree("2018-06-29", "2018-06-29")
        root.geometry("1200x700")

        # self.member_id = 0
        # self.nomdoc = 0

        # --Настройки--
        # Поиск в столбце по умолчанию
        self.head_col_sort = "Счет"
        self.step = 500
        self.disk_save = "T:\\"
        self.ds_name = 'KODAK Scanner: i1100'
        self.user_login = getpass.getuser()
        self.lvl_document = 0
        self.nomdoc = None
        self.member_id = None
        self.last_item = []

    def _setup_widgets(self):
        container = ttk.Frame()
        container.pack(fill='both', expand=True)

        # Создание TreeView
        self.docs_header = ['Отсканировано', 'ID счета', 'Счет', 'Дата счета', 'ID Клиента', 'Клиент',
                            'ИНН', 'ID документа', 'Вид документа', 'Накладная', 'Статус']
        self.tree = ttk.Treeview(columns=self.docs_header, show="headings")
        self.tree.bind('<<TreeviewSelect>>', self.on_tree_click)
        vsb = ttk.Scrollbar(orient="vertical",
                            command=self.tree.yview)
        hsb = ttk.Scrollbar(orient="horizontal",
                            command=self.tree.xview)
        self.tree.configure(yscrollcommand=vsb.set,
                            xscrollcommand=hsb.set)
        self.tree.grid(column=0, row=0, sticky='nsew', in_=container)
        vsb.grid(column=1, row=0, sticky='ns', in_=container)
        hsb.grid(column=0, row=1, sticky='ew', in_=container)
        container.grid_columnconfigure(0, weight=1)
        container.grid_rowconfigure(0, weight=1)

        # Создание Entry поля для поиска
        # self.v_search_entry_tree = ttk.StringVar()
        self.search_entry_tree = ttk.Entry()
        self.search_entry_tree.grid(column=0, row=2, ipadx=10, ipady=3, padx=10, pady=5, sticky='w', in_=container)

        self.b_search = Button(text="Найти", command=self._toSearch)
        self.b_search.grid(column=0, row=2, ipadx=20, ipady=3, padx=160, pady=5, sticky='w', in_=container)

        self.b_scan = Button(text="Сканировать", command=self.before_scan)
        self.b_scan.bind('<<Return>>', self.before_scan)
        self.b_scan.grid(column=0, row=2, ipadx=20, ipady=3, padx=280, pady=5, sticky='w', in_=container)

        self.d_scan = Button(text="Удалить", command=self.delete_scan)
        self.d_scan.grid(column=0, row=2, ipadx=20, ipady=3, padx=405, pady=5, sticky='w', in_=container)

        self.date_2 = ttk.Entry()
        self.date_2.grid(column=0, row=2, ipadx=0, ipady=3, padx=250, pady=5, sticky='e', in_=container)

        self.date_1 = ttk.Entry()
        self.date_1.grid(column=0, row=2, ipadx=0, ipady=3, padx=400, pady=5, sticky='e', in_=container)

        self.date_1.insert(0, "2018-06-29")
        self.date_2.insert(0, "2018-06-29")

        self.b_search_date = Button(text="Изменить дату", command=self.qqqq)
        self.b_search_date.grid(column=0, row=2, ipadx=0, ipady=3, padx=540, pady=5, sticky='w', in_=container)

        self.vc_duplex = IntVar()
        self.c_duplex = Checkbutton(text="Дуплекс", offvalue=False, onvalue=True, variable=self.vc_duplex)
        self.c_duplex.grid(column=0, row=2, ipadx=50, ipady=3, padx=50, pady=5, sticky='e', in_=container)

    def qqqq(self):
        self._build_tree(self.date_1.get(), self.date_2.get())

    def _build_tree(self, date1, date2):
        a = {}
        i = 0

        self.tree.delete(*self.tree.get_children())

        for col in self.docs_header:
            self.tree.heading(col, text=col.title(),
                              command=lambda c=col: self.sortby(self.tree, c, 0))
            # adjust the column's width to the header string
            self.tree.column(col,
                             width=tkFont.Font().measure(col.title()))

        docs_values = self.sql_exec("""exec [0R_InvEx_Period] '%s','%s',0,'*','167','     '""" % (date1, date2), 'fetchall')

        for item in docs_values:
            i += 1

            old_scan = self.sql_exec("""SELECT 
                   [file_path]
                  ,[lvl_document]
                  ,[file_name]
              FROM [0R_TS_Dog_Sale]
              where viddoc = '%s' and nomdoc = '%s'""" % (item.viddoc, item.nomdoc), 'fetchall')
            print(old_scan)
            if not old_scan:
                old_scan = "Нет"
            else:
                old_scan = "Есть %s" % (old_scan[0])

            a[i] = (old_scan, item.sch_nomdoc, item.sch_document, item.sch_data, item.member_id, item.member_name,
                    item.member_inn, item.nomdoc, item.viddoc, item.document, item.status)
            self.text = item.member_id, item.nomdoc, item.viddoc
            self.tree.insert('', 'end', text=self.text, values=a[i])

    def sql_exec(self, sql, d):
        try:
            cursor = self.sql_connect()
            if d == "fetchone":
                cursor.execute(sql)
                data = cursor.fetchone()
                if data:
                    data = data[0]
                else:
                    data = -1
            elif d == "fetchall":
                cursor.execute(sql)
                data = cursor.fetchall()
            elif d == "update":
                cursor.execute(sql)
                conn.commit()
                data = ""
            elif d == "insert":
                cursor.execute(sql)
                conn.commit()
                data = ""
            cursor.close()
            conn.close()
            return data
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def _toSearch(self, *args):
        self.tree.selection_remove(self.last_item)
        try:
            self.selections = []
            self.list = " "
            self.info = self.tree.get_children()
            # self.tree.selection_range(0, 1)

            for i in self.info:
                self.info2 = self.tree.set(i)
                self.inf = self.info2[self.head_col_sort]
                self.list = self.list + i + ": " + self.inf + '\n'

                if self.search_entry_tree.get() != "":
                    if self.search_entry_tree.get().lower() == self.inf.lower()[:len(self.search_entry_tree.get())]:
                        self.selections.append(self.inf)
                        self.tree.selection_add(i)
                        self.last_item.append(i)
                        self.tree.see(i)
            self.msg = "{} \n".format(self.list)
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def on_tree_click(self, event):
        if len(self.tree.selection()) > 0:
            selection = self.tree.selection()[0]
            self.tree.focus(selection)
            self.tree.focus_set()
            self.last_item.append(selection)
            items = self.tree.item(self.tree.selection()[0], "text").split(" ")
            self.member_id = items[0]
            self.nomdoc = items[1]
            self.viddoc = items[2]

    def delete_scan(self):
        if self.file_name:
            os.remove(self.rpath + self.file_name)
            self.sql_exec("""delete from [0R_TS_Dog_Sale] 
                            where file_name = '%s' """ % self.file_name, "update")
            self.file_name = None

    def before_scan(self):
        # Только 1 документ можно сканировазть
        if len(self.tree.selection()) == 1:
            if self.nomdoc and self.member_id:
                try:
                    # проверка версии документа, если
                    self.lvl_document = self.sql_exec("""SELECT  TOP 1 [lvl_document]
                                                FROM [0R_TS_Dog_Sale]
                                                where viddoc = '%s' and nomdoc = '%s'
                                                ORDER BY id desc""" % (self.viddoc, self.nomdoc), 'fetchone')
                    if self.lvl_document >= 0:
                        self.lvl_document += 1
                        self.file_name = "%s_%d" % (self.member_id, self.lvl_document)
                    else:
                        self.lvl_document = 0
                        self.file_name = self.member_id

                    nomdoc = self.nomdoc[2:]
                    for i in nomdoc:
                        if i == "0":
                            nomdoc = nomdoc[1:]
                        else:
                            break
                    self.short_nomdoc = int(nomdoc)
                    dir_name = str(self.get_dir_name())
                    self.rpath = self.disk_save + "invoice\\" + dir_name + "\\"
                    self.file_path = "\invoice\\" + dir_name + "\\"

                    # создание директории, если ее нет
                    if not os.path.exists(self.rpath):
                        os.makedirs(self.rpath)
                    self.scan(self.rpath, self.vc_duplex.get(), tempfile.mkdtemp())
                except:
                    messagebox.showerror("Error", traceback.format_exc())
        else:
            messagebox.showerror("Error", "Необходимо выбрать только один документ")

    def after_scan(self):
        try:
            import os
            self.sql_exec("""INSERT INTO [0R_TS_Dog_Sale] (viddoc, nomdoc, user_login, file_path, lvl_document, file_name)
                            VALUES('%s', '%s', '%s', '%s', '%s', '%s')""" % (self.viddoc, self.nomdoc,
                                                                             self.user_login, self.file_path,
                                                                             self.lvl_document, self.file_name),
                          "insert")

            os.startfile(self.rpath + self.file_name)
        except:
            messagebox.showerror("Error", traceback.format_exc())
        else:
            return True

    def get_dir_name(self):
        if self.short_nomdoc <= self.step:
            dir_name = self.step
            self.step = 2000
            return dir_name
        else:
            self.step += 2000

            print(self.step)
            return self.get_dir_name()

    def scan(self, rpath, duplex, outpath, pixel_type='gray', ext_f='.pdf'):
        try:
            result = twain.acquire_mult(outpath,
                                        dpi=150,
                                        frame=(0, 0, 8.17551, 11.45438),  # A4
                                        pixel_type=pixel_type,
                                        parent_window=root,
                                        duplex=duplex,
                                        ds_name=self.ds_name,
                                        )

            if result and ext_f == '.pdf':
                x, y, w, h = 0, 0, 210, 297  # A4
                pdf = FPDF('P', 'mm', 'A4')
                files = [f for f in os.listdir(outpath) if f.endswith('.jpg')]
                for image in files:
                    pdf.add_page()
                    pdf.set_compression(1)
                    pdf.image(''.join([outpath, '\\', image]), x, y, w, h)
                self.img_path = ''.join([rpath, self.file_name + ".pdf"])
                pdf.output(self.img_path, "F")
                shutil.rmtree(outpath)
                self.after_scan()
        except:
            messagebox.showerror("Error", traceback.format_exc())

    def sortby(self, tree, col, descending):
        # Используется для поиска данных по выбранному стобцу
        self.head_col_sort = col
        data = [(tree.set(child, col), child) \
                for child in tree.get_children('')]

        data.sort(reverse=descending)
        for ix, item in enumerate(data):
            tree.move(item[1], '', ix)
        tree.heading(col, command=lambda col=col: self.sortby(tree, col, int(not descending)))


if __name__ == '__main__':
    root = tk.Tk()
    root.title("TwScan")
    listbox = MultiColumnListbox()
    root.mainloop()
